<?php

namespace App\Http\Controllers\Frontend;

use App\Article;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DefaultController extends Controller
{
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function index()
    {
        $last_10_articles = Article::where('is_active', 1)->orderBy('id', 'desc')->take(10)->get();

        return view('frontend.default.index', compact('last_10_articles'));
    }

    public function detail($slug)
    {
        $article = Article::where('slug', $slug)->first();

        return view('frontend.default.detail', compact('article'));
    }
}
