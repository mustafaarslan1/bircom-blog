<?php

namespace App\Http\Controllers\Backend\Article;

use App\Article;
use App\Http\Controllers\Controller;
use App\Rules\ArticleID;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Yajra\DataTables\DataTables;

class ArticleController extends Controller
{
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function index()
    {
        $page_title = "Makaleler";
        $page_description = "Makaleleri görüntüleyip işlem yapabilirsiniz";

        return view('backend.articles.index', compact('page_description', 'page_title'));
    }

    public function json()
    {
        $user = $this->request->user();

        if ($user->user_group_id == 1){
            $article_list = Article::with('user')->get();
            $is_admin = 1;
        }else{
            $article_list = Article::with('user')->where('user_id', $user->id)->get();
            $is_admin = 0;
        }




        return Datatables::of($article_list)
            ->addColumn('delete_allowed', function($article_list){
                if ($article_list->user_id == $this->request->user()->id || $this->request->user()->user_group_id == 1){
                    return 1;
                }else{
                    return 0;
                }
            })->addColumn('edit_allowed', function($article_list){
                if ($article_list->user_id == $this->request->user()->id || $this->request->user()->user_group_id == 1){
                    return 1;
                }else{
                    return 0;
                }
            })->addColumn('created_formatted', function($article_list){
                return Carbon::parse($article_list->created_at)->format('d-m-Y');
            })->addColumn('is_admin', function() use ($is_admin){
                return $is_admin;
            })->make(true);
    }

    public function add()
    {
        $page_title = 'Yeni Makale Ekleme';
        $page_description = 'Yeni makale ekleyebilirsiniz.';

        return view('backend.articles.add', compact('page_description','page_title'));
    }

    public function update($article_id)
    {
        $article = Article::find($article_id);

        $page_title = 'Makale Düzenle';
        $page_description = 'Seçilen makaleyi düzenleyebilirsiniz.';

        return view('backend.articles.add', compact('article', 'page_title', 'page_description'));
    }

    public function save(Request $request)
    {
        $data = $this->request->all();

        $validator = Validator::make($data, [
            'title' => 'required',
            'content' => 'required',
            'status' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Lütfen tüm zorunlu alanları doldurun',
                'errors' => $validator->errors(),
            ]);
        }

        if ($request->hasFile('file'))
        {
            $request->validate([
                'file' => 'nullable|image|mimes:jpg,jpeg,png|max:2048'
            ]);

            $file_name=uniqid().'.'.$data['file']->getClientOriginalExtension();
            $data['file']->move(public_path('images/blogs'),$file_name);
        } else {
            $file_name=null;
        }

        $slug = Str::slug($data['title']).uniqid();


        if (isset($data['id']))
        {
            $validator = Validator::make($data, [
                'id' => new ArticleID(),
            ]);

            if ($validator->fails()) {
                return response()->json([
                    'message' => 'Bu makaleyi düzenleme yetkiniz yok!',
                    'errors' => $validator->errors(),
                ]);
            }

            $article = Article::find($data['id']);
        }
        else
        {
            $article = new Article();
        }
        $article->title = $data['title'];
        $article->slug = $slug;
        $article->content = $data['content'];
        $article->is_active = $data['status'];
        $article->user_id = Auth::user()->id;
        $article->file = $file_name;
        $article->save();

        $result = array(
            'status' => 1,
            'redirect' => route('articles'),
            'message' => 'Başarıyla kaydettiniz.'
        );
        return response()->json($result);
    }

    public function delete($article_id)
    {
        $delete_article = Article::find($article_id);
        $delete_article->delete();

        $result = array(
            'status' => 1,
            'message' => 'Başarıyla sildiniz.'
        );
        return response()->json($result);

    }

    public function active($article_id)
    {
        $active_article = Article::find($article_id);
        $active_article->is_active = 1;
        $active_article->save();

        $result = array(
            'status' => 1,
            'message' => 'Başarıyla aktife aldınız.'
        );
        return response()->json($result);

    }

    public function passive($article_id)
    {
        $passive_article = Article::find($article_id);
        $passive_article->is_active = 0;
        $passive_article->save();

        $result = array(
            'status' => 1,
            'message' => 'Başarıyla pasife aldınız.'
        );
        return response()->json($result);

    }
}
