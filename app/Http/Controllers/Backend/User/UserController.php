<?php

namespace App\Http\Controllers\Backend\User;

use App\Article;
use App\Http\Controllers\Controller;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Yajra\DataTables\DataTables;

class UserController extends Controller
{
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function index()
    {
        $user = $this->request->user();
        $parameters = $this->request->query();

        $page_title = "Yazarlar";

        if ($user->user_group_id == 1){
            $page_description = "Yazarları görüntüleyip, işlem yapabilirsiniz.";
        }else{
            $page_description = "Yazarları görüntüleyebilirsiniz";
        }

        if (isset($parameters['waiting_users'])) {
            $page_title = 'Onay Bekleyen Kullanıcılar';
        }


        return view('backend.users.index', compact('page_description', 'page_title'));
    }

    public function json()
    {
        $user = $this->request->user();
        $parameters = $this->request->query();

        $user_list = User::where('user_group_id', '!=', 1)
            ->withCount('article')
            ->get();

        if (isset($parameters['waiting_users'])) {
            $user_list = $user_list->where('is_confirmed', 0);
        }

        if ($user->user_group_id == 1){
            $is_admin = 1;
        }else{
            $is_admin = 0;
        }

        return Datatables::of($user_list)
            ->addColumn('created_formatted', function($user_list){
                return Carbon::parse($user_list->created_at)->format('d-m-Y');
            })->addColumn('is_admin', function() use ($is_admin){
                return $is_admin;
            })->make(true);
    }

    public function delete($user_id)
    {
        $delete_user = User::find($user_id);
        $delete_user->delete();

        $result = array(
            'status' => 1,
            'message' => 'Başarıyla sildiniz.'
        );
        return response()->json($result);

    }

    public function confirm($user_id)
    {
        $confirm_user = User::find($user_id);
        $confirm_user->is_confirmed = 1;
        $confirm_user->save();

        $result = array(
            'status' => 1,
            'message' => 'Başarıyla onayladınız aldınız.'
        );
        return response()->json($result);

    }

    public function passive($user_id)
    {
        $passive_user = User::find($user_id);
        $passive_user->is_active = 0;
        $passive_user->save();

        $result = array(
            'status' => 1,
            'message' => 'Başarıyla pasife aldınız.'
        );
        return response()->json($result);

    }

    public function active($user_id)
    {
        $active_user = User::find($user_id);
        $active_user->is_active = 1;
        $active_user->save();

        $result = array(
            'status' => 1,
            'message' => 'Başarıyla aktife aldınız.'
        );
        return response()->json($result);

    }

    public function profile($user_id)
    {
        $detail = User::find($user_id);

        $page_title = 'Profil Düzenle';
        $page_description = 'Profil bilgilerinizi düzenleyebilirsiniz.';

        return view('backend.users.profile', compact('detail', 'page_title', 'page_description'));
    }

    public function saveProfile()
    {
        $data = $this->request->all();

        $validator = Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['nullable', 'string', 'min:8', 'confirmed'],
        ]);

        $user = User::find($data['id']);
        $user->name = $data['name'];
        $user->email = $data['email'];
        if ($data['password']){
            $user->password = Hash::make($data['password']);
        }
        $user->save();

        $result = array(
            'status' => 1,
            'redirect' => route('articles'),
            'message' => 'Başarıyla kaydettiniz.'
        );
        return response()->json($result);

    }
}
