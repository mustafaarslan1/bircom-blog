<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::namespace('Frontend')->group(function (){
    Route::get('/', 'DefaultController@index')->name('default');
    Route::get('/detail/{slug?}', 'DefaultController@detail')->name('article-detail');
});

Route::group(['middleware' => ['auth']], function (){

    Route::prefix('admin')->group(function (){
        Route::get('/', 'HomeController@home')->name('home');
        Route::get('/home', 'HomeController@index')->name('to-home');

        Route::namespace('Backend\Article')->prefix('articles')->group(function (){
            Route::get('/', 'ArticleController@index')->name('articles');
            Route::get('/json', 'ArticleController@json')->name('articles-json');
            Route::get('/add', 'ArticleController@add')->name('add-article');
            Route::get('/add/{id?}', 'ArticleController@update')->name('update-article');
            Route::post('/save', 'ArticleController@save')->name('save-article');
            Route::get('/delete/{id?}', 'ArticleController@delete')->name('delete-article');
            Route::get('/active/{id?}', 'ArticleController@active')->name('active-article');
            Route::get('/passive/{id?}', 'ArticleController@passive')->name('passive-article');
        });

        Route::namespace('Backend\User')->prefix('users')->group(function (){
            Route::get('/', 'UserController@index')->name('users');
            Route::get('/json', 'UserController@json')->name('users-json');
            Route::get('/delete/{id?}', 'UserController@delete')->name('delete-user');
            Route::get('/active/{id?}', 'UserController@active')->name('active-user');
            Route::get('/passive/{id?}', 'UserController@passive')->name('passive-user');
            Route::get('/confirm/{id?}', 'UserController@confirm')->name('confirm-user');
            Route::get('/profile/{id?}', 'UserController@profile')->name('profile');
            Route::post('/save/profile/{id?}', 'UserController@saveProfile')->name('save-profile');
        });
    });

});

Auth::routes();

