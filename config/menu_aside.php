<?php
// Aside menu
return [

    'items' => [
        [
            'title' => 'Yazılar',
            'root' => true,
            'icon' => 'media/svg/icons/Design/Layers.svg', // or can be 'flaticon-home' or any flaticon-*
            'page' => '/admin/articles',
            'new-tab' => false,
        ],

        [
            'title' => 'Kullanıcılar',
            'root' => true,
            'icon' => 'media/svg/icons/Design/Layers.svg', // or can be 'flaticon-home' or any flaticon-*
            'page' => '/admin/users',
            'new-tab' => false,
        ],


    ]

];
