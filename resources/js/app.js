require('./bootstrap');

// custom select2
$('#kt_datatable_search_status').select2();
$('#kt_datatable_search_type').select2();

$('.general-form').ajaxForm({
    beforeSubmit:  function(formData, jqForm, options){
        var val = null;
        $(".formprogress").show();
        $(".my-loader").addClass('active');
        $( ".required", jqForm ).each(function( index ) {
            if(!$(this).val()){
                val = 1;
                $(this).addClass('is-invalid').addClass('is-invalid').closest('.form-group').find('.invalid-feedback').show().html('');
                $(this).closest('.form-group').find('.invalid-feedback').html("Bu alan zorunludur.");
                $(this).closest('.form-group').addClass('invalid-select');
            }else{
                $(this).removeClass('is-invalid');
                $(this).closest('.form-group').removeClass('invalid-select');
                $(this).closest('.form-group').find('.invalid-feedback').html(".");
            }
        });
        if(val){
            KTUtil.scrollTop();
        }
    },
    error: function(){
        $(".formprogress").hide();
        $(".my-loader").removeClass('active');
        swal.fire({
            text: "Dikkat! Sistemsel bir hata nedeniyle kaydedilemedi!",
            icon: "error",
            buttonsStyling: false,
            confirmButtonText: "Tamam",
            customClass: {
                confirmButton: "btn font-weight-bold btn-light-primary"
            }
        }).then(function() {
            KTUtil.scrollTop();
        });
    },
    dataType:  'json',
    success:   function(item){
        $(".my-loader").removeClass('active');
        $(".formprogress").hide();
        if(item.status){
            swal.fire({
                html: item.message,
                icon: "success",
                buttonsStyling: false,
                confirmButtonText: "Tamam",
                customClass: {
                    confirmButton: "btn font-weight-bold btn-light-primary"
                }
            }).then(function() {
                if(item.redirect){
                    window.location.href = item.redirect;
                }else if(item.modal){
                    $('.modal').modal('hide');
                }else if(item.modal){

                }else{
                    location.reload();
                }
            });
        }else{
            $('.is-invalid').removeClass('is-invalid').closest('.form-group').find('.invalid-feedback').hide();
            $('.is-invalid').removeClass('is-invalid').closest('.form-group').removeClass('.invalid-select');
            $.each(item.errors, function(key, value) {
                $("[name="+key+"]").addClass('is-invalid').closest('.form-group').find('.invalid-feedback').show().html('');
                $.each(value, function(k, v) {
                    $("[name="+key+"]").closest('.form-group').addClass('invalid-select').find('.invalid-feedback').append(v + "<br>");
                });
            });

            swal.fire({
                html: item.message,
                icon: "error",
                buttonsStyling: false,
                confirmButtonText: "Tamam",
                customClass: {
                    confirmButton: "btn font-weight-bold btn-light-primary"
                }
            }).then(function() {
                KTUtil.scrollTop();
            });
        }
    }
});
