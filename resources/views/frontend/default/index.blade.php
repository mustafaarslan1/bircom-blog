@extends('frontend.layout')

@section('title', "Anasayfa")

@section('content')



    <!-- Page Content -->
    <div class="container">

        <h1 class="my-4">Bircom Blog Sitesine Hoşgeldiniz</h1>


        <!-- Portfolio Section -->
        <h2>Son Yayınlanan Makaleler</h2>

        <div class="row">
            @foreach($last_10_articles as $article)
                <div class="col-lg-4 col-sm-6 portfolio-item">
                    <div class="card h-100">
                        <a href="{{route('article-detail', ['slug' => $article->slug])}}"><img class="card-img-top" style="max-height: 200px;" src="/images/blogs/{{$article->file}}" alt=""></a>
                        <div class="card-body">
                            <h4 class="card-title">
                                <a href="{{route('article-detail', ['slug' => $article->slug])}}">{{$article->title}}</a>
                            </h4>
                            <p class="card-text">{!! strlen($article->content) > 200 ? substr($article->content, 0, 200)."..." : $article->content !!}</p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <!-- /.row -->

        <!-- Features Section -->
        <!-- /.row -->

        <hr>

        <!-- Call to Action Section -->
    </div>
    <!-- /.container -->

@endsection

