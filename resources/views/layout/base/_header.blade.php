{{-- Header --}}
<div id="kt_header" class="header {{ Metronic::printClasses('header', false) }}" {{ Metronic::printAttrs('header') }}>

    {{-- Container --}}
    <div class="container-fluid d-flex align-items-center justify-content-end">
        <a href="{{ route('profile', ['id' => \Illuminate\Support\Facades\Auth::user()->id]) }}" class="btn btn-light-primary font-weight-bolder">
            {{ Metronic::getSVG("media/svg/icons/General/User.svg", "svg-icon svg-icon-md") }}
            Profilim
        </a>&nbsp;
        <form action="{{route('logout')}}" class="general-form" method="post" enctype="multipart/form-data">
            @csrf

                <div align="right">
                    <button type="submit" class="btn btn-danger">Çıkış Yap</button>
                </div>
        </form>
    </div>
</div>
