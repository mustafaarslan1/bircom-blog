{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')

{{-- Dashboard 1 --}}

<style>
    body.waiting * {
        cursor: wait !important;
    }
</style>

	<!--begin::Card-->
	<div class="card card-custom gutter-b">
        <div class="card-header flex-wrap py-3">
            <div class="card-title">
                <h3 class="card-label">{{ $page_title }}
                    <span class="d-block text-muted pt-2 font-size-sm"></span></h3>
            </div>
            <div class="card-toolbar">
                <!--begin::Button-->
                @if(!Request::get('waiting_users'))
                    <a href="{{ route('users', ['waiting_users' => true]) }}" class="btn btn-light-warning font-weight-bolder">
                        {{ Metronic::getSVG("media/svg/icons/Code/Warning-1-circle.svg", "svg-icon svg-icon-md") }}
                        Onay Bekleyen Kullanıcılar
                    </a>
                @endif    &nbsp;&nbsp;
                @if(Request::get('waiting_users'))
                    <a href="{{ route('users') }}" class="btn btn-light-success btn-bold">Tümünü Göster</a>&nbsp;&nbsp;&nbsp;
                @endif
            </div>
        </div>
		<div class="card-body">
			<!--begin: Datatable-->
			<table class="table table-striped- table-hover table-checkable" id="users">
				<thead>
					<tr>
                        <th>Yazar Adı</th>
                        <th>Yazar Mail</th>
                        <th>Kayıt Tarihi</th>
                        <th>Durumu</th>
                        <th>Yazı Sayısı</th>
                        <th class="align-right">İşlem</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
			<!--end: Datatable-->
		</div>
	</div>

@endsection

{{-- Styles Section --}}
@section('styles')
    <link href="{{ asset('plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css"/>
@endsection


{{-- Scripts Section --}}
@section('scripts')
		<!--begin::Page Vendors(used by this page)-->
    <script src="{{ asset('plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
		<script>
    var costtable = $('#users').DataTable({
        "responsive": true,
        "processing": true,
        "serverSide": true,
        "deferRender": true,
        "ajax": "{{ route('users-json') }}?{!! \Request::getQueryString() !!}",
        dom: `<'row'<'col-sm-6 text-left'f><'col-sm-6 text-right'B>>
        <'row'<'col-sm-12'tr>>
        <'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
                pageLength: 50,
                lengthMenu: [[50, 100, 500, 1000], [50, 100, 500, 1000]],
        "language": {
        "url":"https://cdn.datatables.net/plug-ins/1.10.20/i18n/Turkish.json"
        },
		"order": [[ 0, "desc" ]],
		"ordering": false,
        "drawCallback": function( settings ) {
            $('[data-toggle="tooltip"]').tooltip()
        },
        columns: [
            { data: 'name',  name: 'name', "render": function (data, type, row) {
                return '<span class="font-weight-bolder">'+ data +'</span>';
                }
            },
            { data: 'email', name: 'email', "render": function (data, type, row) {
                    return '<span class="font-weight-bold">'+ data +'</span>';
                }
            },
            { data: 'created_formatted', name: 'created_formatted', "render": function (data, type, row) {

                    return '<span class="label label-lg font-weight-bold  label-light-info label-inline">'+ data +'</span>';
                }
            },
            { data: 'is_active', name: 'is_active', "render": function (data, type, row) {
                    if (data == 1){
                        return '<span class="label label-lg font-weight-bold  label-light-success label-inline">Aktif</span>';
                    }else{
                        return '<span class="label label-lg font-weight-bold  label-light-danger label-inline">Pasif</span>';
                    }
                }
            },

            { data: 'article_count', name: 'article_count', "render": function (data, type, row) {

                    return '<span class="label label-lg font-weight-bold  label-light-primary label-inline">'+ data +'</span>';
                }
            },

            { data: 'id', className:"align-right", name: 'id', "render": function(data, type, row) {
				var deleteme = '<a href="{{route('delete-user')}}/'+ data +'" class="btn btn-icon btn-light btn-hover-danger btn-sm delete-article" data-toggle="tooltip" data-theme="light" title="Sil">\
                    <i class="flaticon2-trash icon-md text-danger"></i>\
					</a>&nbsp;&nbsp;';
				var confirmme = '<a href="{{route('confirm-user')}}/'+ data +'" class="btn btn btn-icon btn-light btn-hover-primary btn-sm delete-article" data-toggle="tooltip" data-theme="light" title="Kabul Et">\
                    <i class="flaticon2-plus text-success"></i>\
					</a>&nbsp;&nbsp;';
				var activeme = '<a href="{{route('active-user')}}/'+ data +'" class="btn btn btn-icon btn-light btn-hover-primary btn-sm delete-article" data-toggle="tooltip" data-theme="light" title="Aktife Al">\
                    <i class="flaticon2-check-mark icon-md text-success"></i>\
					</a>&nbsp;&nbsp;';
                var passiveme = '<a href="{{route('passive-user')}}/'+ data +'" class="btn btn btn-icon btn-light btn-hover-primary btn-sm delete-article" data-toggle="tooltip" data-theme="light" title="Pasife Al">\
                <i class="flaticon2-cancel icon-md text-warning"></i>\
                </a>&nbsp;&nbsp;';
                var waitingme = '<div class="btn btn-icon btn-lg mr-1 pulse pulse-warning" data-toggle="tooltip" data-theme="light" title="Bekleyen Üyelik">\
                                <i class="icon-md flaticon-warning text-warning"></i>\
                                <span class="pulse-ring"></span>\
                                </div>';

                if (row.is_admin){
                    if (row.is_confirmed){
                        if(row.is_active == 1){
                            var result = passiveme ;
                        }else{
                            var result = activeme ;
                        }
                    }else{
                        var result = waitingme;
                        result += confirmme;
                    }
                    result += deleteme ;
                }else {
                    result = '';
                }


				return  '<div style="white-space:nowrap">'+result+'</div>';
				}
			},
        ],
        buttons: [
            {
                extend: 'print',
                title: '{{ $page_title ?? "Filename" }}',
                exportOptions: {
                    columns: [ 0, 1, 2, 3 ]
                }
            },
            {
                extend: 'copyHtml5',
                title: '{{ $page_title ?? "Filename" }}',
                exportOptions: {
                    columns: [ 0, 1, 2, 3 ]
                }
            },
            {
                extend: 'excelHtml5',
                title: '{{ $page_title ?? "Filename" }}',
                exportOptions: {
                    columns: [ 0, 1, 2, 3 ]
                }
            },
            {
                extend: 'csvHtml5',
                title: '{{ $page_title ?? "Filename" }}',
                exportOptions: {
                    columns: [ 0, 1, 2, 3 ]
                }
            },
            {
                extend: 'pdfHtml5',
                title: '{{ $page_title ?? "Filename" }}',
                exportOptions: {
                    columns: [ 0, 1, 2, 3 ]
                }
            },
        ]
    });

    $("body").on('click', '.delete-article', function(e){
        e.preventDefault();
        var thi = $(this);
        var href = $(this).attr('href');
        swal.fire({
            title: "Emin misiniz?",
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Evet!",
            cancelButtonText: "Hayır, vazgeç!",
            reverseButtons: true
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                    url: href,
                    dataType: 'json',
                    type: 'get',
                    success: function(data){
                        if(data.status){
                            costtable.ajax.reload();
                        }else{
                            swal.fire(
                                "Dikkat",
                                data.message,
                                "error"
                            )
                        }
                    }
                });
            } else if (result.dismiss === "cancel") {

            }
        });
		});

    $('#articles').on('processing.dt',function( e, settings, processing ){
        if (processing){
            $('body').addClass('waiting');
        }else {
            $('body').removeClass('waiting');
        }
    } )

</script>
@endsection
