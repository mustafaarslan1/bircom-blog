{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    <script src="https://cdn.ckeditor.com/4.12.1/standard/ckeditor.js"></script>


    <div class="card card-custom">
        <div class="card-header">
            <h3 class="card-title">
                Profil Bilgilerim
            </h3>
        </div>
        <!--begin::Form-->
        <div class="card-body">
            <form action="{{ route('save-profile') }}" class="general-form" method="POST">
                <input type="hidden" name="id" value="{{ $detail->id ?? null }}">
                @csrf

                <div class="form-group row">
                    <label class="col-xl-3 col-lg-3 col-form-label">İsim Soyisim</label>
                    <div class="col-lg-9 col-xl-6">
                        <input class="form-control form-control-lg form-control-solid" type="text" name="name" value="{{ $detail->name ?? '' }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-xl-3 col-lg-3 col-form-label">E-mail</label>
                    <div class="col-lg-9 col-xl-6">
                        <input class="form-control form-control-lg form-control-solid" type="text" name="email" value="{{ $detail->email ?? '' }}">
                    </div>
                </div>
                <hr>
                <div class="form-group row">
                    <label class="col-xl-3 col-lg-3 col-form-label">Parola</label>
                    <div class="col-lg-9 col-xl-6">
                        <input class="form-control form-control-lg form-control-solid" type="password" name="password">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-3 offset-sk-9">
                        <button class="btn btn-success" type="submit">Kaydet</button>
                    </div>
                </div>
            </form>
        </div>
        <!--end::Form-->
    </div>


@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script>

        $('.general-form').ajaxForm({
            beforeSubmit:  function(formData, jqForm, options){
                var val = null;
                $(".formprogress").show();
                $(".my-loader").addClass('active');
                $( ".required", jqForm ).each(function( index ) {
                    if(!$(this).val()){
                        val = 1;
                        $(this).addClass('is-invalid').addClass('is-invalid').closest('.form-group').find('.invalid-feedback').show().html('');
                        $(this).closest('.form-group').find('.invalid-feedback').html("Bu alan zorunludur.");
                        $(this).closest('.form-group').addClass('invalid-select');
                    }else{
                        $(this).removeClass('is-invalid');
                        $(this).closest('.form-group').removeClass('invalid-select');
                        $(this).closest('.form-group').find('.invalid-feedback').html(".");
                    }
                });
                if(val){
                    KTUtil.scrollTop();
                }
            },
            error: function(){
                $(".formprogress").hide();
                $(".my-loader").removeClass('active');
                swal.fire({
                    text: "Dikkat! Sistemsel bir hata nedeniyle kaydedilemedi!",
                    icon: "error",
                    buttonsStyling: false,
                    confirmButtonText: "Tamam",
                    customClass: {
                        confirmButton: "btn font-weight-bold btn-light-primary"
                    }
                }).then(function() {
                    KTUtil.scrollTop();
                });
            },
            dataType:  'json',
            success:   function(item){
                $(".my-loader").removeClass('active');
                $(".formprogress").hide();
                if(item.status){
                    swal.fire({
                        html: item.message,
                        icon: "success",
                        buttonsStyling: false,
                        confirmButtonText: "Tamam",
                        customClass: {
                            confirmButton: "btn font-weight-bold btn-light-primary"
                        }
                    }).then(function() {
                        if(item.redirect){
                            window.location.href = item.redirect;
                        }else if(item.modal){
                            $('.modal').modal('hide');
                        }else if(item.modal){

                        }else{
                            location.reload();
                        }
                    });
                }else{
                    $('.is-invalid').removeClass('is-invalid').closest('.form-group').find('.invalid-feedback').hide();
                    $('.is-invalid').removeClass('is-invalid').closest('.form-group').removeClass('.invalid-select');
                    $.each(item.errors, function(key, value) {
                        $("[name="+key+"]").addClass('is-invalid').closest('.form-group').find('.invalid-feedback').show().html('');
                        $.each(value, function(k, v) {
                            $("[name="+key+"]").closest('.form-group').addClass('invalid-select').find('.invalid-feedback').append(v + "<br>");
                        });
                    });

                    swal.fire({
                        html: item.message,
                        icon: "error",
                        buttonsStyling: false,
                        confirmButtonText: "Tamam",
                        customClass: {
                            confirmButton: "btn font-weight-bold btn-light-primary"
                        }
                    }).then(function() {
                        KTUtil.scrollTop();
                    });
                }
            }
        });


    </script>

@endsection
