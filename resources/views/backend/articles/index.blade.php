{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')

{{-- Dashboard 1 --}}

<style>
    body.waiting * {
        cursor: wait !important;
    }
</style>

<div class="alert alert-custom alert-white alert-shadow show gutter-b" role="alert">
    <div class="btn btn-icon btn-clean btn-lg mr-1 pulse pulse-primary"><span class="svg-icon svg-icon-primary svg-icon-2x"><!--begin::Svg Icon | path:/var/www/preview.keenthemes.com/metronic/releases/2021-02-01-052524/theme/html/demo1/dist/../src/media/svg/icons/Media/Volume-full.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
            <rect x="0" y="0" width="24" height="24"/>
            <path d="M16.3155516,16.1481997 C15.9540268,16.3503696 15.4970619,16.2211868 15.294892,15.859662 C15.0927222,15.4981371 15.2219049,15.0411723 15.5834298,14.8390024 C16.6045379,14.2679841 17.25,13.1909329 17.25,12 C17.25,10.8178416 16.614096,9.74756859 15.6048775,9.17309861 C15.2448979,8.96819005 15.1191879,8.51025767 15.3240965,8.15027801 C15.529005,7.79029835 15.9869374,7.66458838 16.3469171,7.86949694 C17.8200934,8.70806221 18.75,10.2731632 18.75,12 C18.75,13.7396897 17.8061594,15.3146305 16.3155516,16.1481997 Z M16.788778,19.8892305 C16.4155074,20.068791 15.9673493,19.9117581 15.7877887,19.5384876 C15.6082282,19.165217 15.7652611,18.7170589 16.1385317,18.5374983 C18.6312327,17.3383928 20.25,14.815239 20.25,12 C20.25,9.21171818 18.6622363,6.70862302 16.2061077,5.49544344 C15.8347279,5.31200421 15.682372,4.86223455 15.8658113,4.49085479 C16.0492505,4.11947504 16.4990201,3.96711914 16.8703999,4.15055837 C19.8335314,5.61416684 21.75,8.63546229 21.75,12 C21.75,15.3971108 19.7961591,18.4425397 16.788778,19.8892305 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
            <path d="M7,16 L3.60776773,15.3215535 C2.67291934,15.1345839 2,14.3137542 2,13.3603922 L2,10.6396078 C2,9.68624577 2.67291934,8.86541613 3.60776773,8.67844645 L7,8 L10.2928932,4.70710678 C10.6834175,4.31658249 11.3165825,4.31658249 11.7071068,4.70710678 C11.8946432,4.89464316 12,5.14899707 12,5.41421356 L12,18.5857864 C12,19.1380712 11.5522847,19.5857864 11,19.5857864 C10.7347835,19.5857864 10.4804296,19.4804296 10.2928932,19.2928932 L7,16 Z" fill="#000000"/>
        </g>
    </svg><!--end::Svg Icon--></span><span class="pulse-ring"></span></div>
    <div class="alert-text ml-3">
        @if(auth()->user()->user_group_id == 1)
            Bu sayfada tüm yazıları görüntüleyebilir, yazılar üzerinde işlem yapabilirsiniz.
        @else
            Bu sayfada tüm yazılarınızı görüntüleyebilir, yazılar üzerinde işlem yapabilirsiniz.
        @endif
    </div>
    <div class="alert-close">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true"><i class="ki ki-close"></i></span>
        </button>
    </div>
</div>
	<!--begin::Card-->
	<div class="card card-custom gutter-b">
		<div class="card-header flex-wrap py-3">
			<div class="card-title">
				<h3 class="card-label">{{ $page_title }}
				<span class="d-block text-muted pt-2 font-size-sm"></span></h3>
			</div>
            @if(auth()->user()->is_confirmed == 1 )
                <div class="card-toolbar">
                    <!--begin::Button-->
                    <a href="{{ route('add-article') }}" class="btn btn-primary font-weight-bolder">
                    <span class="svg-icon svg-icon-md">
                        <!--begin::Svg Icon | path:assets/media/svg/icons/Design/Flatten.svg-->
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect x="0" y="0" width="24" height="24" />
                                <circle fill="#000000" cx="9" cy="15" r="6" />
                                <path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3" />
                            </g>
                        </svg>
                        <!--end::Svg Icon-->
                    </span>Yeni Yazı Ekle</a>
                    <!--end::Button-->
                </div>
            @else
                <div class="card-toolbar font-weight-bold">
                    Yeni yazı ekleyebilmek için, üyeliğinizin onaylanması gerekmektedir.
                </div>
            @endif

		</div>
		<div class="card-body">
			<!--begin: Datatable-->
			<table class="table table-striped- table-hover table-checkable" id="articles">
				<thead>
					<tr>
                        <th>Yazı Başlığı</th>
                        <th>Kullanıcı Adı</th>
                        <th>Yazı Tarihi</th>
                        <th>Durumu</th>
                        <th class="align-right">İşlem</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
			<!--end: Datatable-->
		</div>
	</div>

@endsection

{{-- Styles Section --}}
@section('styles')
    <link href="{{ asset('plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css"/>
@endsection


{{-- Scripts Section --}}
@section('scripts')
		<!--begin::Page Vendors(used by this page)-->
    <script src="{{ asset('plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
		<script>
    var costtable = $('#articles').DataTable({
        "responsive": true,
        "processing": true,
        "serverSide": true,
        "deferRender": true,
        "ajax": "{{ route('articles-json') }}?{!! \Request::getQueryString() !!}",
        dom: `<'row'<'col-sm-6 text-left'f><'col-sm-6 text-right'B>>
        <'row'<'col-sm-12'tr>>
        <'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
                pageLength: 50,
                lengthMenu: [[50, 100, 500, 1000], [50, 100, 500, 1000]],
        "language": {
        "url":"https://cdn.datatables.net/plug-ins/1.10.20/i18n/Turkish.json"
        },
		"order": [[ 0, "desc" ]],
		"ordering": false,
        "drawCallback": function( settings ) {
            $('[data-toggle="tooltip"]').tooltip()
        },
        columns: [
            { data: 'title',  name: 'title', "render": function (data, type, row) {
                return '<span class="font-weight-bold">'+ data +'</span>';
                }
            },
            { data: 'user.name', name: 'user.name', "render": function (data, type, row) {
                return '<div class="d-flex align-items-center">\
                  <div class="mr-4">\
                    <div class="text-dark-75 font-weight-bolder font-size-lg mb-0">' + data + '</div>\
                    <a href="#" class="text-muted font-weight-bold text-hover-primary">' + row.user.email + '</a>\
                  </div>\
                  </div>';
                }
            },
            { data: 'created_formatted', name: 'created_formatted', "render": function (data, type, row) {

                    return '<span class="label label-lg font-weight-bold  label-light-info label-inline">'+ data +'</span>';
                }
            },
            { data: 'is_active', name: 'is_active', "render": function (data, type, row) {
                    if (data == 1){
                        return '<span class="label label-lg font-weight-bold  label-light-success label-inline">Aktif</span>';
                    }else{
                        return '<span class="label label-lg font-weight-bold  label-light-danger label-inline">Pasif</span>';
                    }
                }
            },

            { data: 'id', className:"align-right", name: 'id', "render": function(data, type, row) {
				var deleteme = '<a href="{{route('delete-article')}}/'+ data +'" class="btn btn-icon btn-light btn-hover-danger btn-sm delete-article" data-toggle="tooltip" data-theme="light" title="Sil">\
                    <i class="flaticon2-trash icon-md text-danger"></i>\
					</a>&nbsp;&nbsp;';
				var editme = '<a href="{{route('update-article')}}/'+ data +'" class="btn btn btn-icon btn-light btn-hover-primary btn-sm" data-toggle="tooltip" data-theme="light" title="Düzenle">\
                    <i class="flaticon2-writing icon-md text-primary"></i>\
					</a>&nbsp;&nbsp;';
				var activeme = '<a href="{{route('active-article')}}/'+ data +'" class="btn btn btn-icon btn-light btn-hover-primary btn-sm delete-article" data-toggle="tooltip" data-theme="light" title="Aktife Al">\
                    <i class="flaticon2-check-mark icon-md text-success"></i>\
					</a>&nbsp;&nbsp;';
                var passiveme = '<a href="{{route('passive-article')}}/'+ data +'" class="btn btn btn-icon btn-light btn-hover-primary btn-sm delete-article" data-toggle="tooltip" data-theme="light" title="Pasife Al">\
                <i class="flaticon2-cancel icon-md text-warning"></i>\
                </a>&nbsp;&nbsp;';

				var result = row.edit_allowed ? editme : '';
                    if(row.is_active == 1){
                        result += row.is_admin ? passiveme : '';
                    }else{
                        result += row.is_admin ? activeme : '';
                    }
				result += row.delete_allowed ? deleteme : '';

				return  '<div style="white-space:nowrap">'+result+'</div>';
				}
			},
        ],
        buttons: [
            {
                extend: 'print',
                title: '{{ $page_title ?? "Filename" }}',
                exportOptions: {
                    columns: [ 0, 1, 2, 3 ]
                }
            },
            {
                extend: 'copyHtml5',
                title: '{{ $page_title ?? "Filename" }}',
                exportOptions: {
                    columns: [ 0, 1, 2, 3 ]
                }
            },
            {
                extend: 'excelHtml5',
                title: '{{ $page_title ?? "Filename" }}',
                exportOptions: {
                    columns: [ 0, 1, 2, 3 ]
                }
            },
            {
                extend: 'csvHtml5',
                title: '{{ $page_title ?? "Filename" }}',
                exportOptions: {
                    columns: [ 0, 1, 2, 3 ]
                }
            },
            {
                extend: 'pdfHtml5',
                title: '{{ $page_title ?? "Filename" }}',
                exportOptions: {
                    columns: [ 0, 1, 2, 3 ]
                }
            },
        ]
    });

    $("body").on('click', '.delete-article', function(e){
        e.preventDefault();
        var thi = $(this);
        var href = $(this).attr('href');
        swal.fire({
            title: "Emin misiniz?",
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Evet!",
            cancelButtonText: "Hayır, vazgeç!",
            reverseButtons: true
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                    url: href,
                    dataType: 'json',
                    type: 'get',
                    success: function(data){
                        if(data.status){
                            costtable.ajax.reload();
                        }else{
                            swal.fire(
                                "Dikkat",
                                data.message,
                                "error"
                            )
                        }
                    }
                });
            } else if (result.dismiss === "cancel") {

            }
        });
		});

    $('#articles').on('processing.dt',function( e, settings, processing ){
        if (processing){
            $('body').addClass('waiting');
        }else {
            $('body').removeClass('waiting');
        }
    } )

</script>
@endsection
