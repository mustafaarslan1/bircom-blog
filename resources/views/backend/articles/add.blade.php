{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    <script src="https://cdn.ckeditor.com/4.12.1/standard/ckeditor.js"></script>


    <div class="card card-custom">
        <div class="card-header">
            <h3 class="card-title">
                Yeni Makale
            </h3>
        </div>
        <!--begin::Form-->
        <div class="card-body">
            <form action="{{route('save-article')}}" class="general-form" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="id" value="{{ $article->id ?? null }}">

                @if(isset($article) && isset($article->file))
                    <div class="form-group">
                        <div class="col-md-6">
                            <div><label>Yüklü Görsel</label></div>
                            <img width="150" src="/images/blogs/{{$article->file}}" alt="">
                        </div>
                    </div>
                @endisset
                <div class="form-group">
                    <div class="col-md-6">
                        <label>Resim Seç</label>
                        <input class="form-control" name="file"  type="file">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6">
                        <label>Başlık</label>
                        <input class="form-control" type="text" name="title" value="{{$article->title ?? null}}">
                    </div>
                </div>


                <div class="form-group">
                    <div class="col-md-8">
                        <label>İçerik</label>
                        <textarea class="form-control" id="editor1"
                                          name="content">{{$article->content ?? null}}</textarea>
                        <script>
                            CKEDITOR.replace('editor1');
                        </script>

                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-3">
                        <label>Durum</label>
                        <select name="status" class="form-control">
                            <option value="1">Aktif</option>
                            <option value="0">Pasif</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div align="left" class="box-footer">
                        <button type="submit" class="btn btn-success">Ekle</button>
                    </div>
                </div>


            </form>
        </div>
        <!--end::Form-->
    </div>


@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script>

        $('.general-form').ajaxForm({
            beforeSubmit:  function(formData, jqForm, options){
                var val = null;
                $(".formprogress").show();
                $(".my-loader").addClass('active');
                $( ".required", jqForm ).each(function( index ) {
                    if(!$(this).val()){
                        val = 1;
                        $(this).addClass('is-invalid').addClass('is-invalid').closest('.form-group').find('.invalid-feedback').show().html('');
                        $(this).closest('.form-group').find('.invalid-feedback').html("Bu alan zorunludur.");
                        $(this).closest('.form-group').addClass('invalid-select');
                    }else{
                        $(this).removeClass('is-invalid');
                        $(this).closest('.form-group').removeClass('invalid-select');
                        $(this).closest('.form-group').find('.invalid-feedback').html(".");
                    }
                });
                if(val){
                    KTUtil.scrollTop();
                }

                for ( instance in CKEDITOR.instances ) {
                    CKEDITOR.instances[instance].updateElement();
                }
            },
            error: function(){
                $(".formprogress").hide();
                $(".my-loader").removeClass('active');
                swal.fire({
                    text: "Dikkat! Sistemsel bir hata nedeniyle kaydedilemedi!",
                    icon: "error",
                    buttonsStyling: false,
                    confirmButtonText: "Tamam",
                    customClass: {
                        confirmButton: "btn font-weight-bold btn-light-primary"
                    }
                }).then(function() {
                    KTUtil.scrollTop();
                });
            },
            dataType:  'json',
            success:   function(item){
                $(".my-loader").removeClass('active');
                $(".formprogress").hide();
                if(item.status){
                    swal.fire({
                        html: item.message,
                        icon: "success",
                        buttonsStyling: false,
                        confirmButtonText: "Tamam",
                        customClass: {
                            confirmButton: "btn font-weight-bold btn-light-primary"
                        }
                    }).then(function() {
                        if(item.redirect){
                            window.location.href = item.redirect;
                        }else if(item.modal){
                            $('.modal').modal('hide');
                        }else if(item.modal){

                        }else{
                            location.reload();
                        }
                    });
                }else{
                    $('.is-invalid').removeClass('is-invalid').closest('.form-group').find('.invalid-feedback').hide();
                    $('.is-invalid').removeClass('is-invalid').closest('.form-group').removeClass('.invalid-select');
                    $.each(item.errors, function(key, value) {
                        $("[name="+key+"]").addClass('is-invalid').closest('.form-group').find('.invalid-feedback').show().html('');
                        $.each(value, function(k, v) {
                            $("[name="+key+"]").closest('.form-group').addClass('invalid-select').find('.invalid-feedback').append(v + "<br>");
                        });
                    });

                    swal.fire({
                        html: item.message,
                        icon: "error",
                        buttonsStyling: false,
                        confirmButtonText: "Tamam",
                        customClass: {
                            confirmButton: "btn font-weight-bold btn-light-primary"
                        }
                    }).then(function() {
                        KTUtil.scrollTop();
                    });
                }
            },
            beforeSerialize:function($Form, options){
                /* Before serialize */
                for ( instance in CKEDITOR.instances ) {
                    CKEDITOR.instances[instance].updateElement();
                }
                return true;
            },
        });


    </script>

@endsection
