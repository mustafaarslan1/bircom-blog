<?php

use Illuminate\Database\Seeder;

class UserGroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $groups = [
            [
                'id' => 1,
                'role' => 'Admin',
                'description' => 'Blog admin kullanıcısı'
            ],
            [
                'id' => 2,
                'role' => 'Author',
                'description' => 'Blog yazar kullanıcısı'
            ],
        ];

        foreach ($groups as $group) {
            $check = null;
            $check = DB::table('user_groups')->where('id', $group['id'])->first();
            if ($check === null) {
                $group['created_at'] = now();
                $group['updated_at'] = now();
                DB::table('user_groups')->insert($group);
            } else {
                $group['updated_at'] = now();
                DB::table('user_groups')->where('id', $check->id)->update($group);
            }
        }
    }
}
