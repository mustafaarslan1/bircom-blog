<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $groups = [
            [
                'id' => 1,
                'name' => 'Admin',
                'email' => 'admin@blog.com',
                'password' => bcrypt('123456'),
                'user_group_id' => 1,
            ],
            [
                'id' => 2,
                'name' => 'Ahmet Yılmaz',
                'email' => 'ahmet@blog.com',
                'password' => bcrypt('654321'),
                'user_group_id' => 2,
            ],
        ];

        foreach ($groups as $group) {
            $check = null;
            $check = DB::table('users')->where('id', $group['id'])->first();
            if ($check === null) {
                $group['created_at'] = now();
                $group['updated_at'] = now();
                DB::table('users')->insert($group);
            } else {
                $group['updated_at'] = now();
                DB::table('users')->where('id', $check->id)->update($group);
            }
        }
    }
}
